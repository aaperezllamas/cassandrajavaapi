package edu.comillas.mibd;

import com.datastax.oss.driver.api.core.CqlIdentifier;
import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import com.datastax.oss.driver.api.core.metadata.Metadata;
import com.datastax.oss.driver.api.core.metadata.Node;

import java.net.InetSocketAddress;
import java.util.Map;
import java.util.UUID;

public class AA_ConectionExample {
    public static void main(String[] args){

        //Si no se especifica ningún nodo se conecta al puerto 9042 de la maquina en la que se ejecuta
        try (CqlSession session = CqlSession.builder().build()) { //Crea un objeto sesion
           ResultSet rs = session.execute("SELECT release_version FROM system.local"); //Indicamos la sentencia que queremos
           Row row = rs.one(); //Que me devuelva el primero y lo guarda en eun objeto row
           System.out.println(row.getString("release_version"));

           //Aqui obetenemos el metadato, que es al informacion del cluster.
           Metadata meta = session.getMetadata();
           Map<UUID, Node> nodes=  meta.getNodes();
            //Iteramos para todos los nodos que tengo la informacion de cada uno de los nodos
           for(Map.Entry<UUID, Node> it : nodes.entrySet()){
               System.out.printf("UUID: %s; listenAddress: %s;\n",
                       it.getKey().toString(), it.getValue().getListenAddress().toString());
            }
        }


        // Para definir un nodo al que conectarse. A un nodo especifico.
        // //si vamos a una ip determinada hay que pasarle el datacenter siempre.
        InetSocketAddress isa = InetSocketAddress.createUnresolved("127.0.0.1", 9042);

        //para conocer el datacenter ejecutar en cql "select data_center from system.local";
        CqlSession session = CqlSession.builder()
                .withLocalDatacenter("datacenter1") //indicamos el datacenter
                .addContactPoint(isa)
                .build(); // construye la sesicon
        ResultSet rs = session.execute("SELECT release_version FROM system.local"); //esto ya es igual que arriba
        Row row = rs.one();
        System.out.println(row.getString("release_version"));
        //Se cierra la sesión
        session.close();


        //se puede fijar el key Space por defecto en la sesion
        CqlSession session2 = CqlSession.builder()
                .withKeyspace(CqlIdentifier.fromCql("redfija"))  //Esto es para indicar el use de una tabla. y no tener que estar diciendo la tabla
                .build();
        //Se cierra la sesión
        session2.close();


        System.out.println("FIN\n\n");

    }
}
